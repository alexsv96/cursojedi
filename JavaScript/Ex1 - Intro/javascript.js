function quadrat(intA){
	return intA*intA;
}

function par(intA){
	if (intA%2 == 0) return "Número par";
	else return "Número impar";
}

function quadrado(intHeight, intWidth){
	if (intHeight == intWidth) return "Es un cuadrado";
	else return "Es un rectángulo";
}

function factorial(intA){
	if (intA == 1) return 1;
	else return (factorial(intA - 1) * intA);
}

function maximo(intA, intB, intC, intD){
	var res;
	if (intA > intB) res = intA;
	else res = intB;

	if (res < intC) res = intC;
	if (res < intD) res = intD; 

	return res;
}

function MCD(intA, intB){
	var sobra = intA % intB;
	if (sobra == 0) return intB;
	else return MCD(intB, sobra);
}

function MCM(intA, intB){
	return (intA*intB)/MCD(intA,intB);
}